﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class DrawLine : MonoBehaviour  
{    
	private TrailRenderer line;    
	private bool isMousePressed;    
	private List<Vector2> pointsList;    
	private Vector2 mousePos;  
	private bool readyForLock;
	private bool inDrawMode;

	private Texture2D texture;
	public Texture2D tex;
	// Structure for line points    
	struct myLine
	{
		public Vector2 StartPoint;         
		public Vector2 EndPoint;    
	};    
	//    -----------------------------------        
	void Awake()    
	{        
		// Create line renderer component and set its property         
		line = gameObject.GetComponent<TrailRenderer>();         
		//line.material =  new Material(Shader.Find("Particles/Additive")); 
		//line.startWidth=0.5;
		//line.endWidth =0.5;
		//line.
		//line.SetVertexCount(0);        
		//line.SetWidth(0.1f,0.1f);         
		//line.SetColors(Color.blue, Color.green);         
		//line.
		//line.useWorldSpace = true;            
		isMousePressed = false;        
		pointsList = new List<Vector2>(); 

		inDrawMode = true;

	}   


	void Start()
	{
		//texture = Texture2D.blackTexture;
		texture = new Texture2D (Screen.width, Screen.height, TextureFormat.ARGB32, false);
		tex = (Texture2D)Resources.Load("Images/triangle");
		GameObject.Find("Tex").renderer.material.mainTexture = tex;
	}

	//    -----------------------------------        
	void Update ()      
	{ 

						// If mouse button down, remove old line and set its color to green        
						if (Input.GetMouseButtonDown (0)) {            
								isMousePressed = true; 
								readyForLock = false;
								//line.SetVertexCount(0);             
								//pointsList.RemoveRange(0,pointsList.Count);             
								//line.SetColors(Color.green, Color.green);        
						} else if (Input.GetMouseButtonUp (0)) {            
								isMousePressed = false;  
								//line.SetVertexCount(0);
								pointsList = new List<Vector2>();// pointsList.RemoveRange (0, pointsList.Count);
						}        
						// Drawing line when mouse is moving(presses)         
						if (isMousePressed) {   
								Vector3 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);  
								mousePos.x = pos.x;
								mousePos.y = pos.y;
			print (mousePos.x+";"+mousePos.y);
								gameObject.transform.Translate (mousePos.x - gameObject.transform.position.x, mousePos.y - gameObject.transform.position.y, 0);
								//mousePos.z=0;           
			if (!pointsList.Contains (mousePos)) {                
										pointsList.Add (mousePos);                 
										    
								}  
								if (Vector2.Distance (pointsList [pointsList.Count - 1], pointsList [0]) < 1) {
										if (readyForLock) {
												//readyForLock = true;
												print ("Lock");
												bool result = checkResult(pointsList,0.7f,0.3f);
												if(result) 
												{
													print("Epic Win");
													Camera.main.GetComponent<MenuAndLogic>().NextRound();
												}
													else print ("epic fail");
												isMousePressed = false;  
												readyForLock = false;
												//line.SetVertexCount(0);
												pointsList= new List<Vector2>(); //.RemoveRange (0, pointsList.Count);
										}
								} else {
										if (!readyForLock) {
												readyForLock = true;
										}
								}

						}
				  
	}    
	//    -----------------------------------        
	//  Following method checks is currentLine(line drawn by last two points) collided with line      
	//    -----------------------------------        
	private bool isLineCollide()    
	{        
		if (pointsList.Count < 2)            
			return false;        
		int TotalLines = pointsList.Count - 1;        
		myLine[] lines = new myLine[TotalLines];        
		if (TotalLines > 1)          
		{            
			for (int i=0; i<TotalLines; i++)              
			{                
				lines [i].StartPoint = (Vector3)pointsList [i];                 
				lines [i].EndPoint = (Vector3)pointsList [i + 1];             
			}        
		}        
		for (int i=0; i<TotalLines-1; i++)          
		{            
			myLine currentLine;            
			currentLine.StartPoint = (Vector3)pointsList [pointsList.Count - 2];            
			currentLine.EndPoint = (Vector3)pointsList [pointsList.Count - 1];            
			if (isLinesIntersect (lines [i], currentLine))                  
				return true;        
		}        
		return false;    
	}    

	private bool checkResult(List<Vector2> points, float max_scale, float min_scale)
	{
		//find max and min to 

		float maxX, maxY, minX, minY;
		maxX = minX = points [0].x;
		maxY = minY = points [0].y;
		foreach (Vector2 p in points) 
		{
			if (p.x>maxX) maxX = p.x; 
			else if (p.x<minX) minX = p.x;

			if (p.y>maxY) maxY = p.y; 
			else if (p.y<minY) minY = p.y;
		}

		float width = maxX - minX; 
		float height = maxY - minY;

		float dx = maxX - width / 2;
		float dy = maxY - height / 2;

		float scale = (width > height) ? width : height; 

		for (int i=0; i<points.Count;i++) 
		{
			Vector2 temp; 
			temp.x = (points[i].x+dx)/scale;
			temp.y = (points[i].y+dy)/scale;
			points[i] = temp;
			//p.x = p.x - scale/2;

			//points[i].y = temp; 
			//p.y = scale/2 - p.y;
		}

		//adoptation for max image


		for (int i=0; i<points.Count;i++) 
		{
			float tempX_max, tempY_max;
			float tempX_min, tempY_min;
			tempX_max = 0.5f*points[i].x/max_scale;
			tempY_max = 0.5f*points[i].y/max_scale;

			tempX_max = tempX_max + 0.5f;
			tempY_max = tempY_max + 0.5f;

			tempX_min = 0.5f*points[i].x/min_scale;
			tempY_min = 0.5f*points[i].y/min_scale;
			
			tempX_min = tempX_min + 0.5f;
			tempY_min = tempY_min + 0.5f;

			scale = tex.height;
			float max_a = tex.GetPixel((int)(tempX_max*scale),(int)(tempY_max*scale)).a;
			float min_a = tex.GetPixel((int)(tempX_min*scale),(int)(tempY_min*scale)).a;
			if(tempX_min<0 || tempY_min<0 || tempX_min>1 || tempY_min>1) min_a = 0;
			if(tempX_max<0 || tempY_max<0 || tempX_max>1 || tempY_max>1) max_a = 0;
			if ( max_a==1 && min_a==0 )
			{
				print (tempX_max+";"+tempY_max+" "+tempX_min+";"+tempY_min);
				print ("OK");
			}
			else 
			{
				print (tempX_max+";"+tempY_max+" "+tempX_min+";"+tempY_min);
				return false;
			}
		}


		return true;
	}

	//    -----------------------------------        
	//    Following method checks whether given two points are same or not    
	//    -----------------------------------        
	private bool checkPoints (Vector3 pointA, Vector3 pointB)     
	{        
		return (pointA.x == pointB.x && pointA.y == pointB.y);     
	}    
	//    -----------------------------------        
	//    Following method checks whether given two line intersect or not    
	//    -----------------------------------        
	private bool isLinesIntersect (myLine L1, myLine L2)    
	{        
		if (checkPoints (L1.StartPoint, L2.StartPoint) || checkPoints (L1.StartPoint, L2.EndPoint) || checkPoints (L1.EndPoint, L2.StartPoint) || checkPoints (L1.EndPoint, L2.EndPoint))            
			return false;        
		return((Mathf.Max (L1.StartPoint.x, L1.EndPoint.x) >= Mathf.Min (L2.StartPoint.x, L2.EndPoint.x)) &&(Mathf.Max (L2.StartPoint.x, L2.EndPoint.x) >= Mathf.Min (L1.StartPoint.x, L1.EndPoint.x)) && (Mathf.Max (L1.StartPoint.y, L1.EndPoint.y) >= Mathf.Min (L2.StartPoint.y, L2.EndPoint.y)) &&(Mathf.Max (L2.StartPoint.y, L2.EndPoint.y) >= Mathf.Min (L1.StartPoint.y, L1.EndPoint.y)));    
	}


	public void Circle(Texture2D tex, int cx, int cy, int r, Color col)
	{
		int x, y, px, nx, py, ny, d;
		
		for (x = 0; x <= r; x++)
		{
			d = (int)Mathf.Ceil(Mathf.Sqrt(r * r - x * x));
			for (y = 0; y <= d; y++)
			{
				px = cx + x;
				nx = cx - x;
				py = cy + y;
				ny = cy - y;
				
				tex.SetPixel(px, py, col);
				tex.SetPixel(nx, py, col);
				
				tex.SetPixel(px, ny, col);
				tex.SetPixel(nx, ny, col);
				
			}
		}    
	}
}
///- See more at: http://www.theappguruz.com/tutorial/draw-line-mouse-move-detect-line-collision-unity2d-unity3d/#sthash.o4k1h2EH.dpuf
