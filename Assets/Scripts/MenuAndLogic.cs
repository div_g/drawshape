﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuAndLogic : MonoBehaviour {

	public float timeForFirstRound;
	private float timeForCurrentRound;
	public float delta;
	private float timer;
	private bool inGame;
	private int currentRound;

	public GameObject shapePlane;
	public Button startButton;
	public Button backButton;

	private DrawLine drawLine;
	public Text InfoMessage;  

	// Use this for initialization
	void Start () {
		//timeForFirstRound = 3;
		timeForCurrentRound = timeForFirstRound;
		HideButton(backButton);
		HideText ();
		//shapePlane.SetActive (false);
		//shapePlane = GameObject.Find("Plane");
		//startButton = GameObject.Find ("StartButton");
		//backButton = GameObject.Find ("BackButton");
		HideImage ();
		drawLine = GameObject.Find ("MouseCursor").GetComponent<DrawLine> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (inGame) 
		{
			timer -= Time.deltaTime;
			if (timer<=0)
			{
				GameOver();
			}
		}
	}

	public void GoToStartMenu()
	{
		//startButton.enabled = true;
		//backButton.enabled = false;

		ShowButton (startButton);
		HideButton (backButton);

		HideText ();
		//HideImage ();
		//shapePlane.SetActive (false);
	}

	public void StartGame()
	{
		currentRound = 1;
		HideButton (startButton);
		timer = timeForCurrentRound;
		timeForCurrentRound -= delta;
		//shapePlane.SetActive (true);
		ShowImage ();
		ShowText ();
		inGame = true;
		InfoMessage.text = "Round " + currentRound;
	}
	public void NextRound()
	{
		currentRound++;
		timer = timeForCurrentRound;
		timeForCurrentRound -= delta;
		InfoMessage.text = "Round " + currentRound;
		if (currentRound % 2 == 0) {
						drawLine.tex = (Texture2D)Resources.Load ("Images/square");
						shapePlane.renderer.material.mainTexture = drawLine.tex;
				}
		else if(currentRound%3==0) 
		{drawLine.tex = (Texture2D)Resources.Load("Images/circle");
			
			shapePlane.renderer.material.mainTexture = drawLine.tex;
		}
		else {drawLine.tex = (Texture2D)Resources.Load("Images/triangle");
			
			shapePlane.renderer.material.mainTexture = drawLine.tex;
		}

	}
	public void GameOver()
	{
		inGame = false;
		//shapePlane.SetActive (false);
		HideImage ();
		//backButton.enabled = true;
		ShowButton (backButton);
		InfoMessage.text = "Your score: " + (currentRound-1);
	}

	void HideButton(Button btn)
	{
		btn.transform.Translate (new Vector3(0,500,0));
		//btn.transform.position.y = 500;
	}

	void ShowButton(Button btn)
	{
		btn.transform.Translate (new Vector3(0,-500,0));
		//btn.transform.position.y = 0;
	}

	void HideText()
	{
		InfoMessage.transform.Translate (new Vector3(0,500,0));
		//InfoMessage.transform.position = 500;
	}

	void ShowText()
	{
		InfoMessage.transform.Translate (new Vector3(0,-500,0));
		//InfoMessage.transform.position = 114;
	}

	void HideImage()
	{
		shapePlane.transform.Translate (new Vector3(0,500,0));
	}

	void ShowImage()
	{
		shapePlane.transform.Translate (new Vector3(0,-500,0));
	}
}
